from flask import Flask,render_template,request,redirect,url_for
import re
from urllib.request import urlopen
from operator import itemgetter

app = Flask(__name__)

"""Global variables
"""
limiteDeBusqueda = 5


"""Test Cases
"""
page= ('this is a <a href="http://python.org/">link!</a> >link!</a> ')
page4= ('this is a <a href="http://xkcd.com/353">link!</a> >link!</a> ')
page2= ('this is a page without links!')
page3= ('<p>A hyperlink referring to a document in the same directory as the current one:<a href="address.html">Examples of using address tag</a>.<p>A hyperlink referring to a document elsewhere:<a href="http://www.hut.fi/english.html">HUT</a>.<p>A hyperlink referring to a label in the same document:<a href="#final">final example</a>.<p>A hyperlink referring to a label in another document:<a href="http://www.ncsa.uiuc.edu/General/Internet/WWW/HTMLPrimerP2.html#UR">URL info in HTML Primer</a><P>A link to an image:<a href="../perhe.jpg" TITLE="Yuccas family picture, by Minna">a family picture</a>.<p><A name="final">Finally, this is just text to which you canrefer with a hyperlink.</a>')

"""Graph: dictionary with all the links as keywords, and all their links as second parameter
"""
graph = {}

"""all_links: list of links without repetead links
"""
all_links=set()

"""Get_next_target(): Get the next href reference
"""
def get_next_target(page,inicio):
    pos= page.find('<a href="http://',inicio)

    if pos >= 0:
        pos2= page.find('<a href="',inicio)
        if pos2 >= 0:
            openQuote= page.find('"',pos)
            endQuote=page.find('"',openQuote+1)
            url = page[openQuote+1:endQuote]
            return (url,endQuote)
    else:
        return (None,0)

"""Get_page: Obtain source code of a specific url
"""
def get_page(url):
    try:
        import urllib.request
        return urllib.request.urlopen(url).read().decode('utf-8')
    except:
        return ""


"""Get all links: Obtain all the links of a specific page
"""
def get_all_links(page, inicio):
    pos =0
    auxLinks=[]
    while pos >= 0:
        pos = page.find('<a href=', inicio)
        resultado = get_next_target(page, inicio)
        if resultado[0] != None:
            all_links.add(resultado[0])
            auxLinks.append(resultado[0])
            inicio = resultado[1]+1
        else:
            break
    return  auxLinks


"""CrawlWeb: Given the links in the all_links list, enters each link and repeat process
"""
def crawlweb():
    contadorPaginas = 0
    while (  len(all_links)>0 and contadorPaginas<limiteDeBusqueda ):
        link= all_links.pop()
        print(link)
        siguiente = get_page(link)
        check_source(siguiente,link)
        todosLinks=get_all_links(siguiente,0)
        graph[link]= todosLinks
        contadorPaginas= contadorPaginas+1

    return 0

"""Index: Dictionary of words as keywords, and urls as second parameter
"""
index={}

"""add_to__index: check if the given(keywordS) word is in the index dictionary, if not add the word with the url
    else add just the url
"""
def add_to_index(index, keywordS, url):
    keywordS= keywordS.lower()
    if keywordS in index:
        #Keyword exist add url
        ##if url already added
        if url in index.setdefault(keywordS, []):
            return
        else:
            index.setdefault(keywordS, []).append(url)
        return index
    index.setdefault(keywordS, []).append(url)
    return index

"""English stop words
"""
stop_words_en={}
for line in app.open_resource('static/stop_words.txt'):
    stop_words_en[line.strip()] = True

"""Spanish stopwords
"""
stop_words_sp={}
for line in app.open_resource('static/stop_words_sp.txt'):
    stop_words_sp[line.strip()] = True

"""stopwords: dictionary of stop words to use
"""
stop_words={}

"""Validation of Stopwords Dictionary
"""
auxDic=0
if(auxDic == 2):
    stop_words = stop_words_en
else:
    if(auxDic == 1):
        stop_words = stop_words_sp

"""Clean tags: Well... it clean tags
"""
def clean_tags(source):
    return re.sub('<[^>]*>', '', source)

"""Check_source: Clean the source of punctuations and call add to index
"""
def check_source(source, url):
    source = clean_tags(source)
    source = re.sub(r'[^\w\s]','',source)
    for word in source.split():
        if word not in stop_words:
            add_to_index(index, word, url)
    return index

"""Calculate rank: algorithm to get the ranks of our pages
    in our graph Dictionary
"""
def Calculaterank(pagesGraph):
    d=0.85
    numloops=10
    ranks={}
    lenghtPages=len(pagesGraph)
    for eachPage in pagesGraph:
      ranks[eachPage]=1.0/lenghtPages
    for i in range(0,numloops):
      newranks={}
      for eachPage in pagesGraph:
       newrank=(1-d)/lenghtPages
       for node in pagesGraph:
        if eachPage in pagesGraph[node]:
         newrank=newrank+d*ranks[node]/len(pagesGraph[node])
       newranks[eachPage]=newrank
      ranks=newranks
    return ranks

"""Buscador: Uses getalllinks, crawlweb and calculate rank. It order the final lranked list, and order the
    links of our word dictionary
"""
def buscador(urlSeed):
    get_all_links(urlSeed,0)
    crawlweb()
    rank=Calculaterank(graph)

    #ordenar los mayores a menores
    sorted(rank, key=itemgetter(1))
    rankList =sorted(rank.items(), key=itemgetter(1))
    rankList.reverse()

    listaOrden = []

    for element in rankList:
        listaOrden.append(element[0])

    print(rankList)

    listaAux = []
    listaPalabras=[]
    for wordn in index:
        for word2n in index.setdefault(wordn, []):
            listaPalabras.append(word2n)
        for elementedRanked in rankList:

            if (elementedRanked[0] in listaPalabras):
                listaAux.append(elementedRanked[0])
        wordn=listaAux
        listaPalabras=[]
        listaAux=[]

"""buscarPalabra: Main function, searches a specific string in our words dictionary and return
    a list with all the links containing that word
"""
def buscarPalabra(search_word):
    search_word = search_word.split()
    result_list = []
    buscador(page)
    for word in search_word:
        if word in index:
              for url in index.setdefault(word, []):
                  result_list.append(url)
        else:
            continue
    print(index)
    return result_list

"""index_buscardor: Get all parameters from the UI and call buscarPalabra
    and redirect the user to the result page
"""
@app.route('/',methods=['GET', 'POST'])
def index_buscador():
    if request.method == 'POST':
        palabra = request.form.get('palabra')
        palabra = palabra.lower()
        listaResultados= buscarPalabra(palabra)
        return render_template('resultadosBusqueda.html', listaResultados= listaResultados)
    else:
       return render_template('index.html')


"""index_buscadors:Get the configuration variables
    to be used by index_buscador
"""
@app.route('/config',methods=['GET', 'POST'])
def index_buscadors():
    if request.method == 'POST':

        global limiteDeBusqueda
        limiteDeBusqueda = int(request.form.get('numeroPagina'))
        global page
        page = ('this is  a <a href="http://'+request.form.get('PaginaSemilla')+'">link!</a> >link!</a>')
        global auxDic
        auxDic = request.form.get('optradio')
        print(auxDic)

        return redirect(url_for('index_buscador'))

    else:
        return render_template('configuracion.html')



if __name__ == '__main__':
    app.run()
